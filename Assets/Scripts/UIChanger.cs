using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIChanger : MonoBehaviour
{
    [SerializeField] Animator _anim;

    private void Start()
    {
        GameManager.instance.GameStarts += ChangeOnStart;
        GameManager.instance.GameEnds += ChangeOnEnd;
    }

    private void OnDisable()
    {
        GameManager.instance.GameStarts -= ChangeOnStart;
        GameManager.instance.GameEnds -= ChangeOnEnd;
    }

    private void ChangeOnStart()
    {
        _anim.SetTrigger("Start");
    }

    private void ChangeOnEnd()
    {
        _anim.SetTrigger("End");
    }
}
