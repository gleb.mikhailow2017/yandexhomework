using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopPlayer : MonoBehaviour
{
    [SerializeField] private PlayerController _pl;

    private void Start()
    {
        GameManager.instance.GameStarts += UnStop;
        GameManager.instance.GameEnds += Stop;

        Stop();
    }

    private void OnDisable()
    {
        GameManager.instance.GameStarts -= UnStop;
        GameManager.instance.GameEnds -= Stop;
    }

    private void Stop()
    {
        _pl.enabled = false;
    }

    private void UnStop()
    {
        _pl.enabled = true;
    }
}
