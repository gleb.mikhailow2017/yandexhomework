using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inventory : MonoBehaviour
{
    [SerializeField] private PlayerHoe _hoe;
    [SerializeField] private PlayerScif _scif;
    [SerializeField] private PlayerSeeds _seed;
    [SerializeField] private TextMeshProUGUI _currentInstr;
    [SerializeField] private TextMeshProUGUI _wheat, _berries, _seeds, _bread, _jam, _sandwich;

    public Dictionary<string, int> Loot = new Dictionary<string, int>();

    public static Inventory PlayerInventory;

    private void Awake()
    {
        PlayerInventory = this;
    }

    private void Start()
    {
        Loot["Wheat"] = 0;
        Loot["Seeds"] = 0;
        Loot["Berries"] = 0;
        Loot["Bread"] = 0;
        Loot["Jam"] = 0;
        Loot["Sandwich"] = 0;
        ChangeInstrument(1);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) ChangeInstrument(1);
        if (Input.GetKeyDown(KeyCode.Alpha2)) ChangeInstrument(2);
        if (Input.GetKeyDown(KeyCode.Alpha3)) ChangeInstrument(3);
        if (Input.GetKeyDown(KeyCode.Alpha4)) ChangeInstrument(4);
    }

    private void ChangeInstrument(int choice)
    {
        switch (choice)
        {
            case 1:
                _hoe.enabled = true;
                _scif.enabled = false;
                _seed.enabled = false;
                _currentInstr.text = "������";
                break;
            case 2:
                _hoe.enabled = false;
                _scif.enabled = true;
                _seed.enabled = false;
                _currentInstr.text = "����";
                break;
            case 3:
                _hoe.enabled = false;
                _scif.enabled = false;
                _seed.enabled = true;
                _seed.PlantType = 0;
                _currentInstr.text = "������ �������";
                break;
            case 4:
                _hoe.enabled = false;
                _scif.enabled = false;
                _seed.enabled = true;
                _seed.PlantType = 1;
                _currentInstr.text = "������ ����";
                break;
        }
    }

    public bool ChangeLoot(string name, int value)
    {
        if (!Loot.ContainsKey(name))
            return false;

        if (Loot[name] + value < 0)
            return false;
        
        Loot[name] += value;
        UpdateUI();

        if (Loot["Sandwich"] >= 20) GameManager.instance.EndGame(true);

        return true;
    }

    private void UpdateUI()
    {
        _wheat.text = Loot["Wheat"].ToString();
        _berries.text = Loot["Berries"].ToString();
        _seeds.text = Loot["Seeds"].ToString();
        _bread.text = Loot["Bread"].ToString();
        _jam.text = Loot["Jam"].ToString();
        _sandwich.text = Loot["Sandwich"].ToString();
    }

    public void CraftBread()
    {
        if (ChangeLoot("Wheat", -3))
            ChangeLoot("Bread", 1);
    }

    public void CraftJam()
    {
        if (ChangeLoot("Berries", -3))
            ChangeLoot("Jam", 1);
    }

    public void CraftSandwich()
    {
        if (ChangeLoot("Jam", -2))
        {
            if(ChangeLoot("Bread", -2))
            {
                ChangeLoot("Sandwich", 1);
            }
            else
            {
                ChangeLoot("Bread", 2);
            }
        }
    }
}
