using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class World : MonoBehaviour
{
    [SerializeField] private Vector2Int _worldSize; 
    [SerializeField] private Vector2Int _worldOffset;

    public static Tilemap TileMap;
    public static World World_;

    public List<Tile> GroundPalette;
    public List<Tile> DetailPalette;
    public float[] DetailChances;
    public int DetailsCount;

    public Dictionary<Vector3Int, InteractableTile> InteractableTiles = new Dictionary<Vector3Int, InteractableTile>();
    public Dictionary<Vector3Int, Plant> Plants = new Dictionary<Vector3Int, Plant>();

    public InteractableTile Detail(int ind, bool isBreaking)
    {
        switch (ind)
        {
            case 0:
                return new Grass(isBreaking);
                break;
            case 1:
                return new Bush(isBreaking);
                break;
        }
        return new InteractableTile(isBreaking);
    }

    private void Awake()
    {
        World_ = this;
    }

    private void Start()
    {
        TileMap = GetComponentInChildren<Tilemap>();

        List<Vector3Int> tilePoses1 = new List<Vector3Int>();
        List<Vector3Int> tilePoses2 = new List<Vector3Int>();

        List<Tile> tiles1 = new List<Tile>();
        List<Tile> tiles2 = new List<Tile>();

        int tmpDetailsCount = 0;

        for (int i = 0; i < _worldSize.y; i++)
        {
            for (int j = 0; j < _worldSize.x; j++)
            {
                int ind = i * _worldSize.x + j;
                tiles1.Add(GroundPalette[Random.Range(0, GroundPalette.Count)]);
                tilePoses1.Add(new Vector3Int(j - _worldOffset.x, i - _worldOffset.y, 0));

                if (tmpDetailsCount < DetailsCount)
                {
                    float r = Random.Range(0f, 1f);

                    for (int k = DetailPalette.Count - 1; k >= 0; k--)
                    {
                        if (r < DetailChances[k])
                        {
                            tiles2.Add(DetailPalette[k]);
                            tilePoses2.Add(new Vector3Int(j - _worldOffset.x, i - _worldOffset.y, 1));
                            InteractableTiles[tilePoses2[tmpDetailsCount]] = Detail(k, true);
                            tmpDetailsCount++;
                            break;
                        }
                    }
                }
            }
        }

        TileMap.SetTiles(tilePoses1.ToArray(), tiles1.ToArray());
        TileMap.SetTiles(tilePoses2.ToArray(), tiles2.ToArray());
    }

    public void Interact(Vector3Int tile)
    {
        InteractableTiles[tile].Interact(tile);
        if (InteractableTiles[tile].IsBreaking)
        {
            TileMap.SetTile(tile, new Tile());
            InteractableTiles.Remove(tile);
        }
        if (Plants.ContainsKey(tile))
        {
            Destroy(Plants[tile].gameObject);
            Plants.Remove(tile);
        }
    }

    public void Plant(Vector3Int tile, GameObject plantPref)
    {
        Plant newPlant = GameObject.Instantiate(plantPref.gameObject).GetComponent<Plant>();
        newPlant.Pos = tile;
        Plants[tile] = newPlant;
    }
}
