using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _speed;
    //input script later
    [SerializeField] private KeyCode _forward, _back, _left, _right;
    [SerializeField] private bool[] _moveSides;

    private Vector2Int _facing;
    private Rigidbody2D _rb;

    public Vector2Int Facing => _facing;

    private void Awake()
    {
         _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(_forward)) _moveSides[0] = true;
        if (Input.GetKeyUp(_forward)) _moveSides[0] = false;
        if (Input.GetKeyDown(_back)) _moveSides[1] = true;
        if (Input.GetKeyUp(_back)) _moveSides[1] = false;
        if (Input.GetKeyDown(_left)) _moveSides[2] = true;
        if (Input.GetKeyUp(_left)) _moveSides[2] = false;
        if (Input.GetKeyDown(_right)) _moveSides[3] = true;
        if (Input.GetKeyUp(_right)) _moveSides[3] = false;
        SetSpeed();
    }
    public void SetSpeed()
    {
        if (_moveSides[1]) 
        {  
            _rb.velocity = new Vector2(_rb.velocity.x, -_speed);
            _facing.x = 0;
            _facing.y = -1;
        }
        if (_moveSides[0])
        {
            _rb.velocity = new Vector2(_rb.velocity.x, _speed);
            _facing.x = 0;
            _facing.y = 1;
        }
        if (!_moveSides[1] & !_moveSides[0]) _rb.velocity = new Vector2(_rb.velocity.x, 0);
        if (_moveSides[2])
        {
            _rb.velocity = new Vector2(-_speed, _rb.velocity.y);
            _facing.x = -1;
            _facing.y = 0;
        }
        if (_moveSides[3])
        {
            _rb.velocity = new Vector2(_speed, _rb.velocity.y);
            _facing.x = 1;
            _facing.y = 0;
        }
        if (!_moveSides[3] & !_moveSides[2]) _rb.velocity = new Vector2(0, _rb.velocity.y);
    }
}
