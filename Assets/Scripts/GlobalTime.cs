using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalTime : MonoBehaviour
{
    [SerializeField] private float _tickTime;

    private IEnumerator _time;

    public static GlobalTime GlobalTime_;
    public Action Tick;

    private void Awake()
    {
        GlobalTime_ = this;
    }

    private void Start()
    {
        GameManager.instance.GameStarts += StartGame;
        GameManager.instance.GameEnds += EndGame;
    }

    private void OnDisable()
    {
        GameManager.instance.GameStarts -= StartGame;
        GameManager.instance.GameEnds -= EndGame;
    }

    private void StartGame()
    {
        _time = Time();
        StartCoroutine(_time);
    }

    private void EndGame()
    {
        StopCoroutine(_time);
    }

    private IEnumerator Time()
    {
        while (true)
        {
            yield return new WaitForSeconds(_tickTime);
            Tick?.Invoke();
        }
    }
}
