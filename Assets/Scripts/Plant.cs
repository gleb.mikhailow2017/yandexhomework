using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Plant : MonoBehaviour
{
    [SerializeField] private int _stateChangingTime;
    [SerializeField] private Tile[] _states;
    [SerializeField] private int _type;

    private int _age;

    public Vector3Int Pos;

    private void OnEnable()
    {
        GlobalTime.GlobalTime_.Tick += AddAge;
    }

    private void OnDisable()
    {
        GlobalTime.GlobalTime_.Tick -= AddAge;
    }

    private void Start()
    {
        World.TileMap.SetTile(Pos, _states[0]);
    }

    private void AddAge()
    {
        if(_age/_stateChangingTime < _states.Length - 1)
        {
            _age++;
            if (_age % _stateChangingTime == 0)
            {
                World.TileMap.SetTile(Pos, _states[_age / _stateChangingTime]);
            }
            if(_age / _stateChangingTime == _states.Length - 1)
            {
                switch (_type)
                {
                    case 0:
                        World.World_.InteractableTiles[Pos] = new Grass(true);
                        break;
                    case 1:
                        World.World_.InteractableTiles[Pos] = new Bush(true);
                        break;

                }
            }
        }
    } 
}
