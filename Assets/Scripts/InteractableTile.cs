using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTile : MonoBehaviour
{
    public bool IsBreaking;

    public InteractableTile(bool isBreaking)
    {
        IsBreaking = isBreaking;
    }

    public virtual void Interact(Vector3Int pos)
    {

    }
}

public class Bush : InteractableTile
{
    public Bush(bool isBreaking) : base(isBreaking)
    {
    }

    public override void Interact(Vector3Int pos)
    {
        Inventory.PlayerInventory.ChangeLoot("Berries", Random.Range(1,3));

        pos.z = 0;
        World.TileMap.SetTile(pos, World.World_.GroundPalette[0]);
    }
}

public class Grass : InteractableTile
{
    public Grass(bool isBreaking) : base(isBreaking)
    {
    }

    public override void Interact(Vector3Int pos)
    {
        Inventory.PlayerInventory.ChangeLoot("Seeds", Random.Range(1, 4));
        Inventory.PlayerInventory.ChangeLoot("Wheat", Random.Range(1, 3));

        pos.z = 0;
        World.TileMap.SetTile(pos, World.World_.GroundPalette[1]);
    }
}