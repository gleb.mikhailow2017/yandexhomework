using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerEnvUser : MonoBehaviour
{
    [SerializeField] private Vector2Int _pos;
    [SerializeField] private PlayerController _pl;
    [SerializeField] private Tile _choosedTile;

    private Tilemap _world => World.TileMap;

    public Vector2Int Pos => _pos;

    private Vector2Int _prepos;

    private void Update()
    {
        _pos = new Vector2Int(Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.y)) + _pl.Facing;

        if(_pos != _prepos)
            ChooseTile();
    }

    private void ChooseTile()
    {
        _world.SetTile(new Vector3Int(_prepos.x, _prepos.y, 2), new Tile());
        _world.SetTile(new Vector3Int(_pos.x, _pos.y, 2), _choosedTile);
        _prepos = _pos;
    }
}
