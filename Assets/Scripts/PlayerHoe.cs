using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerHoe : MonoBehaviour
{
    [SerializeField] private PlayerEnvUser _env;
    [SerializeField] private KeyCode _hoe;
    [SerializeField] private Tile _farmingLand;

    private void Update()
    {
        if (Input.GetKeyDown(_hoe)) HoeTile();
    }

    private void HoeTile()
    {
        if(World.World_.GroundPalette.Contains(World.TileMap.GetTile<Tile>(new Vector3Int(_env.Pos.x, _env.Pos.y, 0))) 
            & !World.World_.DetailPalette.Contains(World.TileMap.GetTile<Tile>(new Vector3Int(_env.Pos.x, _env.Pos.y, 1))))
        {
            World.TileMap.SetTile(new Vector3Int(_env.Pos.x, _env.Pos.y, 0), _farmingLand);
        }
    }
}
