using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class GameManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _lastWords;
    [SerializeField] private string _winnig, _losing;

    public static GameManager instance;

    public Action GameStarts, GameEnds;

    private void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        GameStarts?.Invoke();
    }

    public void EndGame(bool win)
    {
        GameEnds?.Invoke();
        _lastWords.text = win ? _winnig : _losing;
    }
}
