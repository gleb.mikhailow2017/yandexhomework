using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerScif : MonoBehaviour
{
    [SerializeField] private PlayerEnvUser _env;
    [SerializeField] private KeyCode _scif;

    private World _world => World.World_;

    private void Update()
    {
        if (Input.GetKeyDown(_scif)) ScifTile();
    }

    private void ScifTile()
    {
        if (_world.DetailPalette.Contains(World.TileMap.GetTile<Tile>(new Vector3Int(_env.Pos.x, _env.Pos.y, 1))))
        {
            World.World_.Interact(new Vector3Int(_env.Pos.x, _env.Pos.y, 1));
        }
    }
}
