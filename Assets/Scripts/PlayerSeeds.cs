using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PlayerSeeds : MonoBehaviour
{
    [SerializeField] private PlayerEnvUser _env;
    [SerializeField] private KeyCode _seed;
    [SerializeField] private Tile _farmingLand;
    [SerializeField] private GameObject[] _plantPrefs;
    [SerializeField] private string[] _plantsSeedNames;

    public int PlantType;
    private void Update()
    {
        if (Input.GetKeyDown(_seed)) SeedTile();
    }

    public void SeedTile()
    {
        if(World.TileMap.GetTile(new(_env.Pos.x, _env.Pos.y, 0)) == _farmingLand
            & !World.World_.Plants.ContainsKey(new(_env.Pos.x, _env.Pos.y, 1))
            & Inventory.PlayerInventory.ChangeLoot(_plantsSeedNames[PlantType], -1))
        {
            World.World_.Plant(new(_env.Pos.x, _env.Pos.y, 1), _plantPrefs[PlantType]);
        }
    }
}
