using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerStats : MonoBehaviour
{
    [SerializeField] private int _hungerValue, _maxHunger;
    [SerializeField] private Image _hungerImage;
    [SerializeField] private TextMeshProUGUI _hungerTMP;

    private Dictionary<string, int> _eatable = new Dictionary<string, int>();

    private void Start()
    {
        GlobalTime.GlobalTime_.Tick += Hung;

        _eatable["Berries"] = 1;
        _eatable["Jam"] = 2;
        _eatable["Bread"] = 5;
        _eatable["sandwich"] = 8;

        UpdateUI();
    }

    private void OnDisable()
    {
        GlobalTime.GlobalTime_.Tick -= Hung;
    }

    public void Eat(string food)
    {
        if(Inventory.PlayerInventory.ChangeLoot(food, -1))
        {
            _hungerValue = Mathf.Clamp(_hungerValue + _eatable[food], 0, _maxHunger);
            UpdateUI();
        }
    }

    public void Hung()
    {
        _hungerValue--;

        if(_hungerValue <= 0)
        {
            GameManager.instance.EndGame(false);
        }
        else
        {
            UpdateUI();
        }
    }

    private void UpdateUI()
    {
        _hungerImage.fillAmount = (float)_hungerValue / (float)_maxHunger;
        _hungerTMP.text = _hungerValue + " / " + _maxHunger;
    }
}
